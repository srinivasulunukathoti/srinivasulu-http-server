// import the http module
const http = require('http')
const { v4: uuid4 } = require('uuid')
try {
    // create the server
    let myServer = http.createServer((request, response) => {
        // check the method and url
        if (request.method == 'GET' && request.url == '/html') {
            // set the content type of head
            response.setHeader('content-Type', 'text/html')
            // write the html response
            response.write(`
            <html>
              <head>
              </head>
              <body>
                  <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                  <p> - Martin Fowler</p>
            
              </body>
            </html>`)
            // end the responce
            response.end()
        } else if (request.method == 'GET' && request.url == '/json') {
            // set the content type
            response.setHeader('content-Type', 'application/json')
            // write the json response
            response.write(`
              {
                  "slideshow": {
                    "author": "Yours Truly",
                    "date": "date of publication",
                    "slides": [
                      {
                        "title": "Wake up to WonderWidgets!",
                        "type": "all"
                      },
                      {
                        "items": [
                          "Why <em>WonderWidgets</em> are great",
                          "Who <em>buys</em> WonderWidgets"
                        ],
                        "title": "Overview",
                        "type": "all"
                      }
                    ],
                    "title": "Sample Slide Show"
                  }
                }`)
            // end the responce
            response.end()
        } else if (request.method == 'GET' && request.url == '/uuid') {
            // set the header
            response.setHeader('content-Type', 'application/json')
            const uuid = uuid4()
            // write the response
            response.write(`${uuid}`)
            // end the server
            response.end()
        } else if (request.method == 'GET' && request.url.includes('/status')) {
            // set the header
            response.setHeader('content-Type', 'text/plain')
            // string to number
            const statusCode = parseInt(request.url.split("/")[2])
            // check number or not
            if (typeof statusCode === 'number') {
                switch (statusCode) {
                    case 100:
                        response.write('Continue')
                        response.end()
                        break;
                    case 200:
                        response.write('Ok')
                        response.end()
                        break;
                    case 300:
                        response.write('Multiple Choices')
                        response.end()
                        break;
                    case 400:
                        response.write('Bad Request')
                        response.end()
                        break;
                    case 500:
                        response.write('Internal Server Error')
                        response.end()
                        break;
                    default:
                        response.write('Eneter valid number')
                        response.end();
                        break;
                }

            }
            else {
                response.write("not anumber")
                response.end()
            }
        } else if (request.method == 'GET' && request.url.includes('/delay')) {
            // set the content of header
                response.setHeader("content-type", "text/plain");
                // take the value from the url
                const time = parseInt(request.url.split("/")[2]);
                // check the number or not
                if (typeof time === "number") {
                    // creating promise
                    new Promise((resolve, reject) => {
                        // set time out
                        setTimeout(() => {
                            resolve("ok");
                        }, time * 1000);
                    })
                        .then((data) => {
                            response.write(data);
                            response.end();
                        })
                        .catch((error) => {
                            response.write(error);
                            response.end();
                        });
                } else {
                    response.write("Not a Number ");
                    response.end();
                }
         } else {
                response.statusCode = 404;
                response.write("Not Found");
                response.end();
            }

    })
    // start the server and port
    myServer.listen('2500', () => {
        console.log('server is running');
    })
} catch (error) {
    console.log(error);
}